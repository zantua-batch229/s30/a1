// 2


db.fruits.aggregate([
    
    {$match:{
        $and: [{price:{$lt:50}},{supplier:"Yellow Farms"}]
    }},
    {$count: "Yellow Farms Lower Than 50"}

])

// 3


db.fruits.aggregate([
    
    {$match: {price:{$lt:30}}},
    {$count: "Prices Lesser Than 30"}

])

// 4 


db.fruits.aggregate([
    
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"Yellow Farm Fruits", avgOfPrices:{$avg:"$price"}}}

])

// 5

db.fruits.aggregate([
    
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Red Farms Inc.", highestPrice:{$max:"$price"}}}

])

// 6

db.fruits.aggregate([
    
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Red Farms Inc.", lowestPrice:{$min:"$price"}}}

])


